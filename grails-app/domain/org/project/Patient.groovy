package org.project

class Patient {
    String firstName
    String middleName
    String lastName
    String fatherName
    Date dob
    String maritalStatus
    String religion
    String nationality
    int age
    static constraints = {
        firstName blank: false, unique: true, nullable: true
        middleName blank: true, unique: true, nullable: true
        lastName blank: true, unique: true, nullable: true
        fatherName blank: true, unique: true, nullable: true
        dob nullable: true
        maritalStatus nullable: true
        religion nullable: true
        nationality nullable: true
        age nullable: true
    }

    String toString() {
        "$firstName, $middleName, $lastName"
    }
}
